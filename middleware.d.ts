// Type definitions for node-sass-koa-middleware
// Project: https://gitlab.com/bumxu/node-sass-koa-middleware

import * as Koa from 'koa';
import * as sass from 'node-sass';

interface Options extends sass.Options {
    /**
     *
     */
    src: string;
    /**
     *
     */
    dest?: string;
    /**
     *
     */
    root?: string;
    /**
     *
     */
    prefix?: string;
    /**
     *
     */
    force?: boolean;
    /**
     *
     */
    debug?: boolean;
    /**
     *
     */
    indentedSyntax?: boolean;
    /**
     *
     */
    response?: boolean;
    /**
     *
     */
    error?: () => void;
}

declare function nodeSassKoaMiddleware(options: Options): Koa.Middleware;

declare namespace nodeSassMiddleware {
}

export = nodeSassKoaMiddleware;
