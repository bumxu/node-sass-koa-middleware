# node-sass-koa-middleware

Koa middleware for [node-sass](https://github.com/sass/node-sass).  
A simple adaption of the [official Connect/Express middleware](https://github.com/sass/node-sass-middleware) for Koa.

[![npm version](https://badge.fury.io/js/node-sass-koa-middleware.svg)](http://badge.fury.io/js/node-sass-koa-middleware)
[![Dependency Status](https://david-dm.org/sass/node-sass-koa-middleware.svg?theme=shields.io)](https://david-dm.org/sass/node-sass-koa-middleware)
[![devDependency Status](https://david-dm.org/sass/node-sass-koa-middleware/dev-status.svg?theme=shields.io)](https://david-dm.org/sass/node-sass-koa-middleware#info=devDependencies)
[![Gitter chat](http://img.shields.io/badge/gitter-sass/node--sass-brightgreen.svg)](https://gitter.im/sass/node-sass)

## Install

```bash
npm i node-sass-koa-middleware
```
Type definitions are included.

## Usage

Recompile `.scss` or `.sass` files automatically for [koa](https://koajs.com/) based http servers.

### Koa example

```javascript
const Koa = require('koa');
const path = require('path');
const serve = require('koa-static');
const sassMiddleware = require('node-sass-koa-middleware');

const app = new Koa();

app.use(sassMiddleware({
    src: __dirname,
    dest: __dirname + '/public',
    debug: true,
    outputStyle: 'compressed',
    prefix: '/prefix'  // Where prefix is at <link rel="stylesheets" href="prefix/style.css"/>
}));

app.use(serve(path.join(__dirname, '/public')));

app.listen(3000);
```

### Options

 - Same options that [node-sass-middleware](https://github.com/sass/node-sass-middleware#options).

  For full list of options see original node-sass project [here](https://github.com/sass/node-sass).

## License

Under MIT license.  
See [LICENSE](https://gitlab.com/bumxu/node-sass-koa-middleware/-/blob/master/LICENSE) for details.
