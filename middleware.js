const sass = require('node-sass-middleware');

module.exports = function (options) {
    // Build connect middleware
    const middleware = sass(options);

    // Return custom middleware
    return (ctx, next) => new Promise((resolve, reject) => {
        // Virtual connect/express response with missing methods
        const expressRes = {
            writeHead: function (status, headers) {
                ctx.set(headers);
                ctx.status = status;
            },
            end: function (data) {
                ctx.body = data;
                resolve();
            }
        };

        try {
            middleware(ctx.req, expressRes, async () => {
                await next();
                resolve();
            });
        } catch (err) {
            reject(err);
        }
    });
};
